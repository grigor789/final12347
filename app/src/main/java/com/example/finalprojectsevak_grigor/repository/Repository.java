package com.example.finalprojectsevak_grigor.repository;


import androidx.lifecycle.LiveData;

import com.example.finalprojectsevak_grigor.Constants;


import com.example.finalprojectsevak_grigor.dataBase.DataDao;
import com.example.finalprojectsevak_grigor.dataBase.DataModelRoom;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.Countries.BaseBallCountries;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.Teams.BaseBallTeams;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.leagues.BaseBallLeagues;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.LeagueB.LeaguesModelB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.countries.CountriesModelB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.teams.Country;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.teams.TeamModelB;


import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Football.CountryModel.CountriesModel;

import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Football.PlayerModel.PlayerModel;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Football.TeamModel.TeamModel;
import com.example.finalprojectsevak_grigor.network.RetrofitInstance;

import java.util.List;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.Response;

public class Repository {
    public Repository() {

    }
/*Football*/
    public Observable<Response<CountriesModel>> getCountries() {

        return new RetrofitInstance().getApi(Constants.BASE_URLCountries).getCountriesList( Constants.header1);

    }

    public Observable<Response<TeamModel>> getTeams() {
        return new RetrofitInstance().getApi(Constants.BASE_URLTeams).getTeams("Bearer " + Constants.TOKEN_Teams);
    }

    public Observable<Response<PlayerModel>> getPlayers() {
        return new RetrofitInstance().getApi(Constants.BASE_URLF).getPlayer(Constants.header1);
    }

    /****/
    /*BasketBall*/
    public Observable<Response<TeamModelB>> getTeamsB() {
        return new RetrofitInstance().getApi(Constants.Base_UrlB).getTeamsB(Constants.header1,
                Constants.header2, "12", "2019-2020");
    }

    public Observable<Response<CountriesModelB>> getCountriesB() {
        return new RetrofitInstance().getApi(Constants.Base_UrlB).getCountriesB(Constants.header1,
                Constants.header2);
    }
    public Observable<Response<LeaguesModelB>> getLeaguesB() {
        return new RetrofitInstance().getApi(Constants.Base_UrlB).getLeagues(Constants.header1);
    }

    /*******/
    /*Baseball*/
    public Observable<Response<BaseBallTeams>> getBaseballTeams() {
        return new RetrofitInstance().getApi(Constants.BaseBall_url).getTeamsBaseball(Constants.header1,
                 "1", "2020");}


  public Observable<Response<BaseBallLeagues>> getLeaguesBaseball(){
        return new RetrofitInstance().getApi(Constants.BaseBall_url).getLeaguesBaseball(Constants.header1);
  }
public  Observable<Response<BaseBallCountries>> getCountriesBaseBall(){
        return new RetrofitInstance().getApi(Constants.BaseBall_url).getCountriesBaseBall(Constants.header1);
    }


    /*ROOM Database*/
    private DataDao dao;

    public Repository(DataDao dao) {
        this.dao = dao;
    }

    public void addDataToRoom(DataModelRoom dataModelRoom) {
        dao.addData(dataModelRoom);
    }

    public void updateDataInRoom(DataModelRoom dataModelRoom) {
        dao.updateData(dataModelRoom);
    }

    public void deleteDataFromRoom(String json) {
        dao.deleteData(json);
    }

    public void deleteAll() {
        dao.deleteAll();
    }

    public LiveData<List<DataModelRoom>> readDataFromRoom() {
        return dao.readData();

    }
    public int checkExistence(String json){
        return dao.checkExistence(json);
    }
}

