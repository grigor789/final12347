package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.football;

public class PlayersFootball {
    private String name;
    private String weight;
    private String height;
    private String foot;
    private String url;

    public PlayersFootball(String name, String weight, String height, String foot, String url) {
        this.name = name;
        this.weight = weight;
        this.height = height;
        this.foot = foot;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getFoot() {
        return foot;
    }

    public void setFoot(String foot) {
        this.foot = foot;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}


