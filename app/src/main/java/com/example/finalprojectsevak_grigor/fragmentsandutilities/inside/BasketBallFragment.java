package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.inside;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.finalprojectsevak_grigor.R;
import com.example.finalprojectsevak_grigor.ViewModels.MainViewModel;
import com.example.finalprojectsevak_grigor.ViewModels.NetworkViewModel;
import com.example.finalprojectsevak_grigor.ViewModels.ViewModelForRoom;
import com.example.finalprojectsevak_grigor.adapters.BaseballAdapter;
import com.example.finalprojectsevak_grigor.adapters.BasketballAdapter;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.leagues.BaseBallLeagues;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.LeagueB.LeaguesModelB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.countries.CountriesModelB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.teams.TeamModelB;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Response;


public class BasketBallFragment extends Fragment {

    private MainViewModel viewModel;
    private NetworkViewModel netViewModel;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private BasketballAdapter basketballAdapter;
    private CompositeDisposable disposable;
    private ViewModelForRoom viewModelForRoom;
    private ProgressBar progressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(getActivity()).get(MainViewModel.class);
        netViewModel = new ViewModelProvider(getActivity()).get(NetworkViewModel.class);
        viewModelForRoom=new ViewModelProvider(getActivity()).get(ViewModelForRoom.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_basketball, container, false);
        inIt(view);
        setClick();
        firstIn();
        return view;
    }

    private void firstIn() {
        progressBar.setVisibility(View.VISIBLE);
        netViewModel.getCountriesB().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new io.reactivex.rxjava3.core.Observer<Response<CountriesModelB>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onNext(@NonNull Response<CountriesModelB> countriesModelBResponse) {
                        if(countriesModelBResponse.code()==200) {
                            progressBar.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            layoutManager = new LinearLayoutManager(getActivity());
                            basketballAdapter = new BasketballAdapter(0, viewModelForRoom);
                            recyclerView.setLayoutManager(layoutManager);
                            recyclerView.setAdapter(basketballAdapter);
                            basketballAdapter.setCountries(countriesModelBResponse.body());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }



    private void inIt(View view) {
        recyclerView = view.findViewById(R.id.recyclerBasketball);
        disposable = new CompositeDisposable();
        progressBar=view.findViewById(R.id.basketBallProgressBar);
    }

    private void setClick() {
        viewModel.getCountriesBClicked().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s != null) {
                    progressBar.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    netViewModel.getCountriesB().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new io.reactivex.rxjava3.core.Observer<Response<CountriesModelB>>() {
                                @Override
                                public void onSubscribe(@NonNull Disposable d) {
                                    disposable.add(d);
                                }

                                @Override
                                public void onNext(@NonNull Response<CountriesModelB> countriesModelBResponse) {
                                    if(countriesModelBResponse.code()==200) {
                                        progressBar.setVisibility(View.GONE);
                                        recyclerView.setVisibility(View.VISIBLE);
                                        layoutManager = new LinearLayoutManager(getActivity());
                                        basketballAdapter = new BasketballAdapter(0, viewModelForRoom);
                                        recyclerView.setLayoutManager(layoutManager);
                                        recyclerView.setAdapter(basketballAdapter);
                                        basketballAdapter.setCountries(countriesModelBResponse.body());
                                    }
                                }

                                @Override
                                public void onError(@NonNull Throwable e) {

                                }

                                @Override
                                public void onComplete() {

                                }
                            });

                }
            }
        });
        viewModel.getLeaguesBClicked().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s != null) {
                    progressBar.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
netViewModel.getLeaguesB().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
        .subscribe(new io.reactivex.rxjava3.core.Observer<Response<LeaguesModelB>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                disposable.add(d);
            }

            @Override
            public void onNext(@NonNull Response<LeaguesModelB> leaguesModelBResponse) {
                if(leaguesModelBResponse.code()==200){
                recyclerView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                layoutManager = new LinearLayoutManager(getActivity());
                basketballAdapter = new BasketballAdapter(2,viewModelForRoom);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(basketballAdapter);
                basketballAdapter.setLeagues(leaguesModelBResponse.body());

            }}

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });

                }
            }
        });
        viewModel.getTeamsBClicked().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s != null) {progressBar.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);

                    netViewModel.getTeamsB().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).
                            subscribe(new io.reactivex.rxjava3.core.Observer<Response<TeamModelB>>() {
                                @Override
                                public void onSubscribe(@NonNull Disposable d) {
                                    disposable.add(d);
                                }

                                @Override
                                public void onNext(@NonNull Response<TeamModelB> teamModelBResponse) {
                                    if(teamModelBResponse.code()==200){
                                    recyclerView.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                    layoutManager = new LinearLayoutManager(getActivity());
                                    basketballAdapter = new BasketballAdapter(1,viewModelForRoom);
                                    recyclerView.setLayoutManager(layoutManager);
                                    recyclerView.setAdapter(basketballAdapter);
                                    basketballAdapter.setTeams(teamModelBResponse.body());
                                }}

                                @Override
                                public void onError(@NonNull Throwable e) {

                                }

                                @Override
                                public void onComplete() {

                                }
                            });


                }

            }
        });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposable.clear();
    }
}