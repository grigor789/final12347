package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.outside;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.example.finalprojectsevak_grigor.R;

import java.util.Objects;


public class SplashFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_splash, container, false);

        setSplashImage(view);
        transferLoginFragment(view);
        return view;
    }



    private void setSplashImage(View view) {
        AppCompatImageView imageView = view.findViewById(R.id.imageSplash);
        ProgressBar progressBar=view.findViewById(R.id.progress_circular);
        setVisibility(progressBar);
    }


    private void setVisibility(ProgressBar progressBar) {
     progressBar.postDelayed(() -> progressBar.setVisibility(View.VISIBLE),1500);


    }
    private void transferLoginFragment(View view) {
        LoginFragment loginFragment=new LoginFragment();
        FragmentManager fragmentManager= Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                fragmentManager.beginTransaction().replace(R.id.container1,
                loginFragment).commitAllowingStateLoss();
            }
        },5000);

    }}




