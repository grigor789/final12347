
package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.teams;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Parameters {

    @SerializedName("league")
    @Expose
    private String league;
    @SerializedName("season")
    @Expose
    private String season;

    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

}
