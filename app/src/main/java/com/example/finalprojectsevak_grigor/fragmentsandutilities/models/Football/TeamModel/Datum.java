
package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Football.TeamModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("legacy_id")
    @Expose
    private Integer legacyId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("short_code")
    @Expose
    private String shortCode;
    @SerializedName("twitter")
    @Expose
    private String twitter;
    @SerializedName("country_id")
    @Expose
    private Integer countryId;
    @SerializedName("national_team")
    @Expose
    private Boolean nationalTeam;
    @SerializedName("founded")
    @Expose
    private Integer founded;
    @SerializedName("logo_path")
    @Expose
    private String logoPath;
    @SerializedName("venue_id")
    @Expose
    private Integer venueId;
    @SerializedName("current_season_id")
    @Expose
    private Integer currentSeasonId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLegacyId() {
        return legacyId;
    }

    public void setLegacyId(Integer legacyId) {
        this.legacyId = legacyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public Boolean getNationalTeam() {
        return nationalTeam;
    }

    public void setNationalTeam(Boolean nationalTeam) {
        this.nationalTeam = nationalTeam;
    }

    public Integer getFounded() {
        return founded;
    }

    public void setFounded(Integer founded) {
        this.founded = founded;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public Integer getVenueId() {
        return venueId;
    }

    public void setVenueId(Integer venueId) {
        this.venueId = venueId;
    }

    public Integer getCurrentSeasonId() {
        return currentSeasonId;
    }

    public void setCurrentSeasonId(Integer currentSeasonId) {
        this.currentSeasonId = currentSeasonId;
    }

}
