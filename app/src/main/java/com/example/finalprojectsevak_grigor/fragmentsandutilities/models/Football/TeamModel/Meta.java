
package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Football.TeamModel;

import java.util.List;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Meta {

    @SerializedName("subscription")
    @Expose
    private Subscription subscription;
    @SerializedName("plan")
    @Expose
    private Plan plan;
    @SerializedName("sports")
    @Expose
    private List<Sport> sports = null;
    @SerializedName("pagination")
    @Expose
    private Pagination pagination;

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }

    public List<Sport> getSports() {
        return sports;
    }

    public void setSports(List<Sport> sports) {
        this.sports = sports;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

}
