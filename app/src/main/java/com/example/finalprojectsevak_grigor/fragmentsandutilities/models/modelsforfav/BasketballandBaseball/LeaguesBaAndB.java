package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.BasketballandBaseball;

public class LeaguesBaAndB {
    private String name;
    private String country;
    private String type;
    private String url;

    public LeaguesBaAndB(String name, String country, String type, String url) {
        this.name = name;
        this.country = country;
        this.type = type;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
