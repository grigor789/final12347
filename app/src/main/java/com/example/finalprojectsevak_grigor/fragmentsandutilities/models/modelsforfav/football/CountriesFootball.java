package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.football;

public class CountriesFootball {
    private String name;
    private String capital;
    private String subRegion;
    private String url;

    public CountriesFootball(String name, String capital, String subRegion, String url) {
        this.name = name;
        this.capital = capital;
        this.subRegion = subRegion;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getSubRegion() {
        return subRegion;
    }

    public void setSubRegion(String subRegion) {
        this.subRegion = subRegion;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
