package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.outside;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.finalprojectsevak_grigor.R;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Firebasemodel.FireBaseModel;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class ForgotFragment extends Fragment implements View.OnClickListener {
    private TextInputLayout username;
    private TextInputLayout password;
    private TextInputLayout newPassword;
    private AppCompatButton confirm;
    private DatabaseReference reference;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forgot, container, false);
        setViews(view);
        return view;
    }

    private void setViews(View view) {
        reference = FirebaseDatabase.getInstance().getReference();
        username = view.findViewById(R.id.forgot_username);
        password = view.findViewById(R.id.forgot_password);
        newPassword = view.findViewById(R.id.new_password);
        confirm = view.findViewById(R.id.btn_confirm);
        confirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (username.isErrorEnabled()) {
            username.setErrorEnabled(false);
        }
        if (password.isErrorEnabled()) {
            password.setErrorEnabled(false);
        }
        if (newPassword.isErrorEnabled()) {
            newPassword.setErrorEnabled(false);
        }

        if (username.getEditText().getText().toString().equals("")) {
            username.setError("Username is Empty ");
        }
        if (password.getEditText().getText().toString().equals("")) {
            password.setError("password is Empty ");

        }
        if (!password.getEditText().getText().toString().equals(newPassword.getEditText().getText().toString())) {
            newPassword.setError("passwords don't match");


        }
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(!username.getEditText().getText().toString().equals("")){
                if(snapshot.hasChild(username.getEditText().getText().toString().trim())&&!password.getEditText().getText().toString().equals("")
                        &&!newPassword.getEditText().getText().toString().equals("")&&
                        password.getEditText().getText().toString().equals(newPassword.getEditText().getText().toString()))
                {
                    FireBaseModel model=snapshot.child(username.getEditText().getText().toString()).getValue(FireBaseModel.class);
                    model.setPassword(password.getEditText().getText().toString());
                    reference.child(username.getEditText().getText().toString()).setValue(model);
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container1,new LoginFragment()).commit();
                }else {username.setError("Invalid Username"); }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}