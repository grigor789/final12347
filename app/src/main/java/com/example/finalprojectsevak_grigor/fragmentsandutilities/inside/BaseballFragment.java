package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.inside;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.finalprojectsevak_grigor.Constants;
import com.example.finalprojectsevak_grigor.R;
import com.example.finalprojectsevak_grigor.ViewModels.MainViewModel;
import com.example.finalprojectsevak_grigor.ViewModels.NetworkViewModel;
import com.example.finalprojectsevak_grigor.ViewModels.ViewModelForRoom;
import com.example.finalprojectsevak_grigor.adapters.BaseballAdapter;
import com.example.finalprojectsevak_grigor.adapters.BasketballAdapter;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.Countries.BaseBallCountries;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.Teams.BaseBallTeams;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.leagues.BaseBallLeagues;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.LeagueB.LeaguesModelB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.teams.TeamModelB;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Response;


public class BaseballFragment extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private BaseballAdapter baseballAdapter;
    private MainViewModel viewModel;
    private NetworkViewModel netViewModel;
    private CompositeDisposable disposable;
    private ViewModelForRoom viewModelForRoom;
    private ProgressBar progressBar;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(getActivity()).get(MainViewModel.class);
        netViewModel = new ViewModelProvider(getActivity()).get(NetworkViewModel.class);
        viewModelForRoom = new ViewModelProvider(getActivity()).get(ViewModelForRoom.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_baseball, container, false);
        inIt(view);
        setClick();
        firstIn();
        return view;
    }

    private void firstIn() {progressBar.setVisibility(View.VISIBLE);


        netViewModel.getCountriesBaseBall().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new io.reactivex.rxjava3.core.Observer<Response<BaseBallCountries>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                disposable.add(d);
            }

            @Override
            public void onNext(@NonNull Response<BaseBallCountries> baseBallCountriesResponse) {
                if(baseBallCountriesResponse.code()==200){
                recyclerView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                layoutManager = new LinearLayoutManager(getActivity());
                baseballAdapter = new BaseballAdapter(0, viewModelForRoom);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(baseballAdapter);
                baseballAdapter.setCountries(baseBallCountriesResponse.body());

            }}

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });

    }


    private void inIt(View view) {
        recyclerView = view.findViewById(R.id.recyclerViewbaseball);
        disposable = new CompositeDisposable();
        progressBar = view.findViewById(R.id.progress_baseball);
    }

    private void setClick() {
        viewModel.getCountriesBaClicked().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s != null) {progressBar.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    netViewModel.getCountriesBaseBall().subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread()).subscribe(new io.reactivex.rxjava3.core.Observer<Response<BaseBallCountries>>() {
                        @Override
                        public void onSubscribe(@NonNull Disposable d) {
                            disposable.add(d);
                        }

                        @Override
                        public void onNext(@NonNull Response<BaseBallCountries> baseBallCountriesResponse) {
                            if(baseBallCountriesResponse.code()==200){
                            progressBar.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            layoutManager = new LinearLayoutManager(getActivity());
                            baseballAdapter = new BaseballAdapter(0, viewModelForRoom);
                            recyclerView.setLayoutManager(layoutManager);
                            recyclerView.setAdapter(baseballAdapter);
                            baseballAdapter.setCountries(baseBallCountriesResponse.body());

                        }}

                        @Override
                        public void onError(@NonNull Throwable e) {

                        }

                        @Override
                        public void onComplete() {

                        }
                    });

                }
            }
        });
        viewModel.getLeaguesBaClicked().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s != null) {progressBar.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    netViewModel.getLeaguesBaseball().subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread()).subscribe(new io.reactivex.rxjava3.core.Observer<Response<BaseBallLeagues>>() {
                        @Override
                        public void onSubscribe(@NonNull Disposable d) {
                            disposable.add(d);
                        }

                        @Override
                        public void onNext(@NonNull Response<BaseBallLeagues> baseBallLeaguesResponse) {
                            if(baseBallLeaguesResponse.code()==200){
                            progressBar.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            layoutManager = new LinearLayoutManager(getActivity());
                            baseballAdapter = new BaseballAdapter(2, viewModelForRoom);
                            recyclerView.setLayoutManager(layoutManager);
                            recyclerView.setAdapter(baseballAdapter);
                            baseballAdapter.setLeagues(baseBallLeaguesResponse.body());
                        }}

                        @Override
                        public void onError(@NonNull Throwable e) {

                        }

                        @Override
                        public void onComplete() {

                        }
                    });

                }
            }
        });
        viewModel.getTeamsBaClicked().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s != null) {
                    progressBar.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    netViewModel.getBaseballTeams().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new io.reactivex.rxjava3.core.Observer<Response<BaseBallTeams>>() {
                                @Override
                                public void onSubscribe(@NonNull Disposable d) {
                                    disposable.add(d);
                                }

                                @Override
                                public void onNext(@NonNull Response<BaseBallTeams> baseBallTeamsResponse) {
                                    if(baseBallTeamsResponse.code()==200){
                                    progressBar.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                    layoutManager = new LinearLayoutManager(getActivity());
                                    baseballAdapter = new BaseballAdapter(1, viewModelForRoom);
                                    recyclerView.setLayoutManager(layoutManager);
                                    recyclerView.setAdapter(baseballAdapter);
                                    baseballAdapter.setTeams(baseBallTeamsResponse.body());
                                }}

                                @Override
                                public void onError(@NonNull Throwable e) {

                                }

                                @Override
                                public void onComplete() {

                                }
                            });

                }

            }
        });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposable.clear();
    }
}
