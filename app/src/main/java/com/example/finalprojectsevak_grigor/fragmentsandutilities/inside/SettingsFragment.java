package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.inside;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.finalprojectsevak_grigor.R;
import com.example.finalprojectsevak_grigor.ViewModels.MainViewModel;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.service.MyService;

import java.util.Objects;


public class SettingsFragment extends Fragment implements View.OnClickListener {

   private AppCompatButton btn_on;
    private AppCompatButton btn_off;
    private Intent intent;
    private MainViewModel viewModel;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view= inflater.inflate(R.layout.fragment_settings, container, false);
       setView(view);
       return view;
    }

    private void setView(View view) {
        viewModel=   new ViewModelProvider(Objects.requireNonNull(getActivity())).get(MainViewModel.class);
        intent=new Intent(getActivity(), MyService.class);

        btn_on = view.findViewById(R.id.btn_volume_on);
        btn_off = view.findViewById(R.id.btn_volume_Off);
        btn_on.setOnClickListener(this);
        btn_off.setOnClickListener(this);
        viewModel.getVolumeView().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if(s!=null){
                    if(s.equals("2")){
                        btn_on.setVisibility(View.VISIBLE);
                        btn_off.setVisibility(View.GONE);
                    }else {btn_off.setVisibility(View.VISIBLE);
                        btn_on.setVisibility(View.GONE);}
                }
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_volume_on:
              //  getActivity().startService(intent);
                viewModel.serVolumeView("1");

                break;
            case R.id.btn_volume_Off:
            //    getActivity().stopService(intent);
                viewModel.serVolumeView("2");
                break;

        }

    }


}