package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.inside;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.finalprojectsevak_grigor.R;
import com.example.finalprojectsevak_grigor.ViewModels.MainViewModel;
import com.example.finalprojectsevak_grigor.ViewModels.NetworkViewModel;
import com.example.finalprojectsevak_grigor.ViewModels.ViewModelForRoom;
import com.example.finalprojectsevak_grigor.adapters.BasketballAdapter;
import com.example.finalprojectsevak_grigor.adapters.FootballAdapter;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Football.CountryModel.CountriesModel;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Football.PlayerModel.PlayerModel;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Football.TeamModel.TeamModel;
import com.google.android.material.textview.MaterialTextView;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Response;


public class FootballFragment extends Fragment {
    private MainViewModel viewModel;
    private NetworkViewModel netViewModel;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FootballAdapter footballAdapter;
    private CompositeDisposable disposable;
    private ViewModelForRoom viewModelForRoom;
    private ProgressBar progressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(getActivity()).get(MainViewModel.class);
        netViewModel = new ViewModelProvider(getActivity()).get(NetworkViewModel.class);
        viewModelForRoom=new ViewModelProvider(getActivity()).get(ViewModelForRoom.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_football, container, false);
        inIt(view);
        setClick();
        firstIn();
        return view;
    }

    private void firstIn() {progressBar.setVisibility(View.VISIBLE);

        netViewModel.getFootballCountries().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(new io.reactivex.rxjava3.core.Observer<Response<CountriesModel>>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {
                    disposable.add(d);
                }

                @Override
                public void onNext(@NonNull Response<CountriesModel> countriesModelResponse) {
                    if(countriesModelResponse.code()==200){
                    progressBar.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    layoutManager = new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(layoutManager);
                    footballAdapter = new FootballAdapter(0,viewModelForRoom);
                    recyclerView.setAdapter(footballAdapter);
                    footballAdapter.setCountries(countriesModelResponse.body());
                }}

                @Override
                public void onError(@NonNull Throwable e) {

                }

                @Override
                public void onComplete() {

                }
            });

    }

    private void inIt(View view) {
        recyclerView = view.findViewById(R.id.recyclerViewFootball);
        disposable = new CompositeDisposable();
progressBar=view.findViewById(R.id.progress_bar_football);

    }

    private void setClick() {
        viewModel.getCountriesFClicked().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s != null) {progressBar.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    netViewModel.getFootballCountries().subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread()).subscribe(new io.reactivex.rxjava3.core.Observer<Response<CountriesModel>>() {
                        @Override
                        public void onSubscribe(@NonNull Disposable d) {
                       disposable.add(d);
                        }

                        @Override
                        public void onNext(@NonNull Response<CountriesModel> countriesModelResponse) {
                            if(countriesModelResponse.code()==200){
                            progressBar.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            layoutManager = new LinearLayoutManager(getActivity());
                            recyclerView.setLayoutManager(layoutManager);
                            footballAdapter = new FootballAdapter(0,viewModelForRoom);
                            recyclerView.setAdapter(footballAdapter);
                            footballAdapter.setCountries(countriesModelResponse.body());
                        }}

                        @Override
                        public void onError(@NonNull Throwable e) {

                        }

                        @Override
                        public void onComplete() {

                        }
                    });

                }
            }
        });
        viewModel.getTeamsFClicked().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s != null) {progressBar.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    netViewModel.getFootballTeams().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new io.reactivex.rxjava3.core.Observer<Response<TeamModel>>() {
                                @Override
                                public void onSubscribe(@NonNull Disposable d) {
                                    disposable.add(d);
                                }

                                @Override
                                public void onNext(@NonNull Response<TeamModel> teamModelResponse) {
                                    if(teamModelResponse.code()==200){
                                    progressBar.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                    layoutManager = new LinearLayoutManager(getActivity());
                                    recyclerView.setLayoutManager(layoutManager);
                                    footballAdapter = new FootballAdapter(1,viewModelForRoom);
                                    recyclerView.setAdapter(footballAdapter);
                                    footballAdapter.setTeams(teamModelResponse.body());

                                }}

                                @Override
                                public void onError(@NonNull Throwable e) {

                                }

                                @Override
                                public void onComplete() {

                                }
                            });

                }
            }
        });
        viewModel.getPlayersFClicked().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s != null) {recyclerView.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    netViewModel.getFootballPlayer().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new io.reactivex.rxjava3.core.Observer<Response<PlayerModel>>() {
                                @Override
                                public void onSubscribe(@NonNull Disposable d) {
                                    disposable.add(d);
                                }

                                @Override
                                public void onNext(@NonNull Response<PlayerModel> playerModelResponse) {
                                    if(playerModelResponse.code()==200) {
                                        progressBar.setVisibility(View.GONE);
                                        recyclerView.setVisibility(View.VISIBLE);
                                        layoutManager = new LinearLayoutManager(getActivity());
                                        recyclerView.setLayoutManager(layoutManager);
                                        footballAdapter = new FootballAdapter(2, viewModelForRoom);
                                        recyclerView.setAdapter(footballAdapter);
                                        footballAdapter.setPlayers(playerModelResponse.body());
                                    }
                                }

                                @Override
                                public void onError(@NonNull Throwable e) {

                                }

                                @Override
                                public void onComplete() {

                                }
                            });

                }

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposable.clear();
    }
}