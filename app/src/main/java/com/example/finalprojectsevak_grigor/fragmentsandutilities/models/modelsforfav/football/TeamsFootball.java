package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.football;

public class TeamsFootball {
    private String name;
    private String shortCode;
    private String founded;
    private String url;

    public TeamsFootball(String name, String shortCode, String founded, String url) {
        this.name = name;
        this.shortCode = shortCode;
        this.founded = founded;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getFounded() {
        return founded;
    }

    public void setFounded(String founded) {
        this.founded = founded;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
