package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.inside;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.finalprojectsevak_grigor.R;
import com.example.finalprojectsevak_grigor.ViewModels.ViewModelForRoom;
import com.example.finalprojectsevak_grigor.adapters.FavoriteAdapter;
import com.example.finalprojectsevak_grigor.dataBase.DataModelRoom;

import java.util.ArrayList;
import java.util.List;


public class FavoriteFragment extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FavoriteAdapter favoriteAdapter;
    private ViewModelForRoom viewModelForRoom;
   private AppCompatButton deleteAll;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view= inflater.inflate(R.layout.fragment_favorite, container, false);
    init(view);
    setData();
       return view;
    }

    private void init(View view) {
        recyclerView=view.findViewById(R.id.recyclerViewFavorite);
        layoutManager=new LinearLayoutManager(getActivity());
        favoriteAdapter=new FavoriteAdapter();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(favoriteAdapter);
        deleteAll=view.findViewById(R.id.btn_deleteAll);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModelForRoom=new ViewModelProvider(getActivity()).get(ViewModelForRoom.class);
    }
    private void setData() {
        viewModelForRoom.readData.observe(getActivity(), new Observer<List<DataModelRoom>>() {
            @Override
            public void onChanged(List<DataModelRoom> dataModelRooms) {
                favoriteAdapter.setData(dataModelRooms);
            }
        });
        deleteAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModelForRoom.deleteALLFromRoom();
            }
        });
    }
}