package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.outside;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.example.finalprojectsevak_grigor.Constants;
import com.example.finalprojectsevak_grigor.R;
import com.example.finalprojectsevak_grigor.ViewModels.MainViewModel;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.inside.HomeFragment;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Firebasemodel.FireBaseModel;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class LoginFragment extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private DatabaseReference reference;
    private TextInputLayout username;
    private TextInputLayout password;
    private AppCompatCheckBox rememberCheckBox;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;
    private MainViewModel viewModel;
    String key;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        setViews(view);
        viewModel=new ViewModelProvider(getActivity()).get(MainViewModel.class);

        viewModel.getLoggedOut().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
              key=(s);
            }
        });


        if (key!=null&&key.equals(Constants.Logged_out)) {
            mEditor.clear();
            mEditor.commit();
        }
        if (!mPreferences.getAll().isEmpty()) {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container1,
                    new HomeFragment()).commit();
        }

        return view;
    }


    private void setViews(View view) {
        mPreferences = getActivity().getSharedPreferences("gui", Context.MODE_PRIVATE);
        mEditor = mPreferences.edit();
        reference = FirebaseDatabase.getInstance().getReference();
        username = view.findViewById(R.id.username_login);
        password = view.findViewById(R.id.password_login);
        AppCompatButton login = view.findViewById(R.id.btn_logIn);
        AppCompatButton register = view.findViewById(R.id.btn_signUp);
        AppCompatTextView forgot = view.findViewById(R.id.btn_forgot);
        login.setOnClickListener(this);
        register.setOnClickListener(this);
        forgot.setOnClickListener(this);
        rememberCheckBox = view.findViewById(R.id.rememberCheckBox);
        rememberCheckBox.setOnCheckedChangeListener(this);

    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_logIn:
                btnLoginClicked();
                break;
            case R.id.btn_signUp:
                btnRegisterClicked();
                break;
            case R.id.btn_forgot:
                btnForgetClicked();
                break;
        }
    }

    private void btnForgetClicked() {
        getActivity().getSupportFragmentManager().
                beginTransaction().
                addToBackStack(null).
                replace(R.id.container1, new ForgotFragment()).commit();

    }

    private void btnRegisterClicked() {
        getActivity().getSupportFragmentManager().
                beginTransaction().
                addToBackStack(null).
                replace(R.id.container1, new RegisterFragment()).commit();

    }

    private void btnLoginClicked() {

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (username.getEditText().getText() != null && !username.getEditText().getText().toString().equals("") &&
                        password.getEditText().getText() != null && !password.getEditText().getText().toString().equals("")) {
                    String user = username.getEditText().getText().toString();
                    String pass = password.getEditText().getText().toString();

                    if (snapshot.hasChild(user)) {
                        FireBaseModel model = snapshot.child(user).getValue(FireBaseModel.class);
                        if (model.getPassword().equals(pass)) {
                            getActivity().getSupportFragmentManager().
                                    beginTransaction().
                                    replace(R.id.container1, new HomeFragment()).commit();
                        } else {
                            if (username.isErrorEnabled()) {
                                username.setErrorEnabled(false);
                            }
                            password.setError(" ");
                        }

                    } else {
                        if (password.isErrorEnabled()) {
                            password.setErrorEnabled(false);
                        }
                        username.setError(" ");
                    }
                } else if (username.getEditText().getText().toString().equals("") && !password.getEditText().getText().toString().equals("")) {
                    if (password.isErrorEnabled()) {
                        password.setErrorEnabled(false);
                    }
                    username.setError(" ");
                } else if (password.getEditText().getText().toString().equals("") && !username.getEditText().getText().toString().equals("")) {
                    if (username.isErrorEnabled()) {
                        username.setErrorEnabled(false);
                    }

                    password.setError(" ");
                } else if (password.getEditText().getText().toString().equals("") && username.getEditText().getText().toString().equals("")) {
                    username.setError(" ");

                    password.setError(" ");
                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (compoundButton.isChecked()) {
            mEditor.putString("username", "1");
            mEditor.putString("password", "2");
            mEditor.commit();

        } else if (!compoundButton.isChecked()) {
            mEditor.clear();

            mEditor.commit();


        }
    }
}





