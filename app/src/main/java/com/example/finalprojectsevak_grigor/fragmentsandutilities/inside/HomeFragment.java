package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.inside;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;


import com.example.finalprojectsevak_grigor.Constants;
import com.example.finalprojectsevak_grigor.R;
import com.example.finalprojectsevak_grigor.ViewModels.MainViewModel;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.outside.LoginFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import java.util.Objects;


public class HomeFragment extends Fragment implements NavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawerLayout;
    private OnBackPressed onBackPressedListener;
    private MainViewModel viewModel;
    private String fragmentPath = Constants.FootballFragment;
   private BottomNavigationView bottomNavigationView;
   private MenuItem itemLeagues;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        setInIt(view);
        return view;
    }

    private void setInIt(View view) {
        viewModel = new ViewModelProvider(Objects.requireNonNull(getActivity())).get(MainViewModel.class);
        drawerLayout = view.findViewById(R.id.drawer_layout);
        NavigationView navigationView = view.findViewById(R.id.navigationView);
        Toolbar toolbar = view.findViewById(R.id.tool_bar);
        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);
        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setDisplayShowTitleEnabled(false);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        setHasOptionsMenu(true);
        onBackPressedListener.onBackPressed(drawerLayout);
        navigationView.setItemIconTintList(null);
         bottomNavigationView = view.findViewById(R.id.bottom_Nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        Menu menu=bottomNavigationView.getMenu();
        itemLeagues=menu.findItem(R.id.leagues);
        itemLeagues.setTitle("Players");
        itemLeagues.setIcon(R.drawable.ic_soccer_player);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container3, new FootballFragment()).commit();


    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.general, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logOut) {
            viewModel.setLogged_out(Constants.Logged_out);
            Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container1, new LoginFragment()).commit();
        }
        if (item.getItemId() == R.id.settings) {
            bottomNavigationView.setVisibility(View.GONE);
            Objects.requireNonNull(getActivity()).getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container3, new SettingsFragment()).commit();

        }
        return super.onOptionsItemSelected(item);
    }

    public interface OnBackPressed {
        void onBackPressed(DrawerLayout drawerLayout);

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            onBackPressedListener = (OnBackPressed) context;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }


    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            /*navigation view*/
            case R.id.settings:
                bottomNavigationView.setVisibility(View.GONE);
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction().
                        replace(R.id.container3, new SettingsFragment()).commit();
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                }
                break;
            case R.id.football_menu:
                bottomNavigationView.setVisibility(View.VISIBLE);
                itemLeagues.setTitle("Players");
                fragmentPath = Constants.FootballFragment;
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container3, new FootballFragment()).commit();
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                }

                break;
            case R.id.basketBall_menu:
                bottomNavigationView.setVisibility(View.VISIBLE);
                fragmentPath = Constants.BasketballFragment;
                itemLeagues.setTitle("Leagues");
                itemLeagues.setIcon(R.drawable.ic_leagues);
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container3, new BasketBallFragment()).commit();
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                }

                break;
            case R.id.baseball_menu:
                bottomNavigationView.setVisibility(View.VISIBLE);
                fragmentPath = Constants.BaseballFragment;
                itemLeagues.setTitle("Leagues");
                itemLeagues.setIcon(R.drawable.ic_leagues);
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container3, new BaseballFragment()).commit();
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                }

                break;
            case R.id.favorite_menu:
                bottomNavigationView.setVisibility(View.GONE);
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container3, new FavoriteFragment()).commit();
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                }
                break;
            /*bottom navigation view*/
            case R.id.countries:
                if (fragmentPath.equals(Constants.FootballFragment)) {
                    viewModel.setCountriesFClicked(Constants.CountriesClicked);
                }
                if (fragmentPath.equals(Constants.BasketballFragment)) {
                    viewModel.setCountriesBClicked(Constants.CountriesClicked);
                }
                if (fragmentPath.equals(Constants.BaseballFragment)) {
                    viewModel.setCountriesBaClicked(Constants.CountriesClicked);
                }
                break;
            case R.id.teams:
                if (fragmentPath.equals(Constants.FootballFragment)) {
                    viewModel.setTeamsFClicked(Constants.TeamsClicked);
                }
                if (fragmentPath.equals(Constants.BasketballFragment)) {
                    viewModel.setTeamsBClicked(Constants.TeamsClicked);
                }
                if (fragmentPath.equals(Constants.BaseballFragment)) {
                    viewModel.setTeamsBaClicked(Constants.TeamsClicked);
                }
                break;
            case R.id.leagues:
                if (fragmentPath.equals(Constants.FootballFragment)) {
                    viewModel.setPlayersFClicked(Constants.LeaguesClicked);
                }
                if (fragmentPath.equals(Constants.BasketballFragment)) {
                    viewModel.setLeaguesBClicked(Constants.LeaguesClicked);
                }
                if (fragmentPath.equals(Constants.BaseballFragment)) {
                    viewModel.setLeaguesBaClicked(Constants.LeaguesClicked);
                }
                break;
        }
        return true;
    }
}