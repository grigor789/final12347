package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.BasketballandBaseball;

public class CountriesBaAndB {
    private String name;
    private String code;
    private String url;

    public CountriesBaAndB(String name, String code, String url) {
        this.name = name;
        this.code = code;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
