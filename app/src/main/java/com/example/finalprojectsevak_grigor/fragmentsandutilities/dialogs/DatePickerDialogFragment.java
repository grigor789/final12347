package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.TimeUtils;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.finalprojectsevak_grigor.ViewModels.MainViewModel;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class DatePickerDialogFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
  private   Calendar c;
MainViewModel mainViewModel;
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
    mainViewModel=new ViewModelProvider(getActivity()).get(MainViewModel.class);
       c =Calendar.getInstance();

        int yy=c.get(Calendar.YEAR);
        int mm=c.get(Calendar.MONTH);
        int dd=c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog= new  DatePickerDialog(getActivity(), this,yy,mm,dd);
        long years8=252455616000L;

    dialog.getDatePicker().setMaxDate(new Date().getTime()-years8);

    return dialog;}

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

        String sessionDate =
                day + "/" +
                        month + "/" +
                        year + "/";
        mainViewModel.setDate(sessionDate);
    }


}

