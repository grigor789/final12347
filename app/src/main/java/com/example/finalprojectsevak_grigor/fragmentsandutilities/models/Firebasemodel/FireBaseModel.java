package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Firebasemodel;

public class FireBaseModel {
    private String name;
    private String password;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private String date;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public FireBaseModel(String name, String password,String date) {
        this.name = name;
        this.password = password;
        this.date=date;
    }

    public FireBaseModel() {

    }
}
