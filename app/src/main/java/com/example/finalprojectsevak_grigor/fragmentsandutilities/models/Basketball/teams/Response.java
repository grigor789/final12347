
package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.teams;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("nationnal")
    @Expose
    private Boolean nationnal;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("country")
    @Expose
    private Country country;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getNationnal() {
        return nationnal;
    }

    public void setNationnal(Boolean nationnal) {
        this.nationnal = nationnal;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

}
