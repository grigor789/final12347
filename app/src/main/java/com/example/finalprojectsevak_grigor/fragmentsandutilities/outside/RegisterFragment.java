package com.example.finalprojectsevak_grigor.fragmentsAndUtilities.outside;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.finalprojectsevak_grigor.R;
import com.example.finalprojectsevak_grigor.ViewModels.MainViewModel;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.dialogs.DatePickerDialogFragment;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Firebasemodel.FireBaseModel;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;


public class RegisterFragment extends Fragment implements View.OnClickListener {


    private DatabaseReference reference;
    private TextInputLayout nameText;
    private TextInputLayout usernameText;
    private TextInputLayout passwordText;
    private TextInputLayout confirmPasswordText;
    private AppCompatTextView birthday;
    private MainViewModel mainViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        insertDataToFirebase();
        initView(view);
        return view;
    }

    private void initView(View view) {
        nameText = view.findViewById(R.id.setUserName);
mainViewModel=new  ViewModelProvider(getActivity()).get(MainViewModel.class);
        usernameText = view.findViewById(R.id.setUserNameLogin);
        passwordText = view.findViewById(R.id.setPassword1);
        confirmPasswordText = view.findViewById(R.id.confirmPassword1);
        birthday = view.findViewById(R.id.birthDate);
        AppCompatButton register = view.findViewById(R.id.btn_register);
        birthday.setOnClickListener(this);
        register.setOnClickListener(this);
    }

    private void insertDataToFirebase() {
        reference = FirebaseDatabase.getInstance().getReference();
    }

    private void addUser() {
        if (nameText.isErrorEnabled()) {
            nameText.setErrorEnabled(false);
        }
        if (usernameText.isErrorEnabled()) {
            usernameText.setErrorEnabled(false);
        }
        if (passwordText.isErrorEnabled()) {
            passwordText.setErrorEnabled(false);
        }
        if (confirmPasswordText.isErrorEnabled()) {
            confirmPasswordText.setErrorEnabled(false);
        }
        String name = Objects.requireNonNull(nameText.getEditText()).getText().toString().trim();
        String username = Objects.requireNonNull(usernameText.getEditText()).getText().toString().trim();
        String password = Objects.requireNonNull(passwordText.getEditText()).getText().toString().trim();
        String confirmPassword = Objects.requireNonNull(confirmPasswordText.getEditText()).getText().toString().trim();
        String birthdayText=birthday.getText().toString().trim();

        if (!password.equals(confirmPassword)) {
            confirmPasswordText.setError("passwords don't match");
        }
        if (name.isEmpty()) {
            nameText.setError("name is empty");
        }

        if (username.isEmpty()) {
            usernameText.setError("username is empty");
        }
        if (password.isEmpty()) {
            passwordText.setError("password is empty");
        }
        if (confirmPassword.isEmpty()) {
            confirmPasswordText.setError("confirmPassword is empty");
        }
        if (birthdayText.isEmpty()){birthday.setError(" ");}
        if (!name.isEmpty() && !username.isEmpty() && !password.isEmpty() && password.equals(confirmPassword)
            &&!birthdayText.isEmpty()
        ) {
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (!snapshot.hasChild(username)) {
                        reference.child(username).setValue(new FireBaseModel(name, password,birthdayText));
                        Objects.requireNonNull(getActivity()).getSupportFragmentManager().popBackStack();
                    } else {
                        usernameText.setError("this username is used");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_register:
                addUser();
                break;
            case R.id.birthDate:
                addBirthDate();


        }
    }

    private void addBirthDate() {
        DatePickerDialogFragment tPicker = new DatePickerDialogFragment();
        tPicker.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), "");
        mainViewModel.getDate().observe(getActivity(), s -> birthday.setText(s));

    }
}

