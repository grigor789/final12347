package com.example.finalprojectsevak_grigor;

public class Constants {
    public static final String FootballFragment = "FootballFragment";
    public static final String BasketballFragment = "BasketballFragment";
    public static final String BaseballFragment = "BaseballFragment";
    public static final String CountriesClicked = "CountriesClicked";
    public static final String LeaguesClicked = "LeaguesClicked";
    public static final String TeamsClicked = "TeamsClicked";
    public static final String BASE_URLF = "https://elenasport-io1.p.rapidapi.com/v2/seasons/1/";
    public static final String BASE_URLCountries = "https://elenasport-io1.p.rapidapi.com/v2/";
    public static final String BASE_URLTeams = "https://soccer.sportmonks.com/api/v2.0/";
    public static final String TOKEN_Teams = "tcDFHl1RPXRh3q3Me3pMf1UdKiM9KDWsuXp9xb8M6wRqGkDvpg1hTVzVcHOG";
    public static final String Logged_out = "Logged_out";
    public static final String TokenString = "Basic " + "NTRuMnA1NXFxOWltMmRzMTkwZWJvZnQ3djU6MWs5Mzg5cnBkNDN2cWwxdjY0NGprc3Y1c2tkbmttMnV1ODY0MmVrc2c4ZmJyc29tODVkZg==";
    public static final String Base_UrlB = "https://api-basketball.p.rapidapi.com/";
    public static final String header1 = "8cd08fdcffmshf4b3cd151952fa6p166feejsna8e09bcab1b9";
    public static final String header2 = "api-basketball.p.rapidapi.com";
    public static final String BaseBall_url = "https://api-baseball.p.rapidapi.com";
    public static final String Table1 = "table1";
    public static final String CountryFootball = "CountryFootball";
    public static final String PlayerFootball = "PlayerFootball";
    public static final String TeamsFootball = "TeamsFootball";
    public static final String CountryBasketball="CountryBasketball";
    public static final String TeamsBasketball="TeamsBasketball";
    public static final String LeaguesBasketball="LeaguesBasketball";
    public static final String CountryBaseball="CountryBaseball";
    public static final String TeamsBaseball="TeamsBaseball";
    public static final String LeaguesBaseball="LeaguesBaseball";


}
