package com.example.finalprojectsevak_grigor.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.finalprojectsevak_grigor.Constants;
import com.example.finalprojectsevak_grigor.R;
import com.example.finalprojectsevak_grigor.ViewModels.ViewModelForRoom;
import com.example.finalprojectsevak_grigor.dataBase.DataModelRoom;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.Countries.BaseBallCountries;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.Teams.BaseBallTeams;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.leagues.BaseBallLeagues;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.LeagueB.LeaguesModelB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.countries.CountriesModelB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.teams.TeamModelB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.BasketballandBaseball.CountriesBaAndB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.BasketballandBaseball.LeaguesBaAndB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.BasketballandBaseball.TeamsBaAndB;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;


public class BasketballAdapter extends RecyclerView.Adapter {
    private int a;
    private static ArrayList<CountriesModelB> listC = new ArrayList<>();
    private static ArrayList<LeaguesModelB> listL = new ArrayList<>();
    private static ArrayList<TeamModelB> listT = new ArrayList<>();
    private static ViewModelForRoom roomViewModel;

    public BasketballAdapter(int a, ViewModelForRoom roomViewModel) {
        this.a = a;
        this.roomViewModel = roomViewModel;
    }

    @Override
    public int getItemViewType(int position) {
        switch (a) {
            case 0:
                return 0;
            case 1:
                return 1;
            case 2:
                return 2;
            default:
                return -1;
        }

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view;
        switch (viewType) {
            case 0:
                view = layoutInflater.inflate(R.layout.countriescardview_b, parent, false);
                return new CountriesHolder(view);
            case 1:
                view = layoutInflater.inflate(R.layout.teamscardview_b, parent, false);
                return new TeamsHolder(view);
            case 2:
                view = layoutInflater.inflate(R.layout.leaguescardview_b, parent, false);
                return new LeaguesHolder(view);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case 0: {
                CountriesHolder countriesHolder = (CountriesHolder) holder;
                countriesHolder.bindC(position);
            }
            break;
            case 1: {
                TeamsHolder teamsHolder = (TeamsHolder) holder;
                teamsHolder.bindT(position);
            }
            break;
            case 2: {
                LeaguesHolder leaguesHolder = (LeaguesHolder) holder;
                leaguesHolder.bindL(position);

            }
            break;
        }


    }

    @Override
    public int getItemCount() {
        switch (a) {
            case 0:
                if (!listC.isEmpty() && !listC.get(0).getResponse().isEmpty()) {
                    return listC.get(0).getResponse().size();
                } else return 0;
            case 1:
                if (!listT.isEmpty() && !listT.get(0).getResponse().isEmpty()) {
                    return listT.get(0).getResponse().size();
                } else return 0;
            case 2:
                if (!listL.isEmpty() && !listL.get(0).getResponse().isEmpty()) {
                    return listL.get(0).getResponse().size();
                } else return 0;
            default:
                return 0;
        }

    }

    static class CountriesHolder extends RecyclerView.ViewHolder {
        private final AppCompatImageView imageView;
        private final MaterialTextView name;
        private final MaterialTextView code;
        private final AppCompatImageView favorite;

        public CountriesHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.cardviewCountryBImage);
            name = itemView.findViewById(R.id.countryNameCardViewB);
            code = itemView.findViewById(R.id.codeCountryBCardView);
            favorite=itemView.findViewById(R.id.favoriteCountriesB);
        }

        void bindC(int position) {
            CountriesModelB countriesModelB = listC.get(0);
            String url = countriesModelB.getResponse().get(position).getFlag();
            String name1 = countriesModelB.getResponse().get(position).getName();
            String code1 = countriesModelB.getResponse().get(position).getCode();
            Glide.with(imageView.getContext()).load(url).into(imageView);
            name.setText(name1);
            code.setText(code1);
            GsonBuilder gsonBuilder=new GsonBuilder();
            String json=gsonBuilder.create().toJson(new CountriesBaAndB(name1,code1,url));
            if(roomViewModel.checkExistence(json)!=0) {favorite.setImageResource(R.drawable.ic_baseline_favorite_24);}else favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
            favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(roomViewModel.checkExistence(json)==0) {
                        favorite.setImageResource(R.drawable.ic_baseline_favorite_24);
                        roomViewModel.addDataToDB(new DataModelRoom(json, Constants.CountryBasketball));
                    }else {favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                        roomViewModel.deleteFromRoom(json);}
                }
            });

        }
    }

    static class LeaguesHolder extends RecyclerView.ViewHolder {
        private final AppCompatImageView imageView;
        private final MaterialTextView name;
        private final MaterialTextView country;
        private final MaterialTextView type;
        private final AppCompatImageView favorite;

        public LeaguesHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.cardviewLeaguesBImage);
            name = itemView.findViewById(R.id.nameLeaguesCardViewB);
            country = itemView.findViewById(R.id.countrylLeagueBCardView);
            type = itemView.findViewById(R.id.cardViewTypeLeagueB);
            favorite = itemView.findViewById(R.id.favoriteLeaguesB);
        }

        void bindL(int position) {
            LeaguesModelB leaguesModelB = listL.get(0);
            String url = leaguesModelB.getResponse().get(position).getLogo();
            String name1 = leaguesModelB.getResponse().get(position).getName();
            String country1 = leaguesModelB.getResponse().get(position).getCountry().getName();
            String type1 = leaguesModelB.getResponse().get(position).getType();
            Glide.with(imageView.getContext()).load(url).into(imageView);
            name.setText(name1);
            country.setText(country1);
            type.setText(type1);
            GsonBuilder gsonBuilder=new GsonBuilder();
            String json=gsonBuilder.create().toJson(new LeaguesBaAndB(name1,country1,type1,url));
            if(roomViewModel.checkExistence(json)!=0) {favorite.setImageResource(R.drawable.ic_baseline_favorite_24);}else favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
            favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(roomViewModel.checkExistence(json)==0) {
                        favorite.setImageResource(R.drawable.ic_baseline_favorite_24);
                        roomViewModel.addDataToDB(new DataModelRoom(json, Constants.LeaguesBasketball));
                    }else {favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                        roomViewModel.deleteFromRoom(json);}
                }
            });
        }
    }

    static class TeamsHolder extends RecyclerView.ViewHolder {
        private final AppCompatImageView imageView;
        private final MaterialTextView name;
        private final MaterialTextView country;
        private final MaterialTextView code;
        private final AppCompatImageView favorite;

        public TeamsHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.cardviewTeamBImage);
            name = itemView.findViewById(R.id.nameTeamCardViewB);
            country = itemView.findViewById(R.id.countryTeamBCardView);
            code = itemView.findViewById(R.id.cardViewTeamBCode);
            favorite = itemView.findViewById(R.id.favoriteTeamB);
        }

        void bindT(int position) {
            TeamModelB teamModelB = listT.get(0);
            String url = teamModelB.getResponse().get(position).getLogo();
            String name1 = teamModelB.getResponse().get(position).getName();
            String country1 = teamModelB.getResponse().get(position).getCountry().getName();
            String code1 = teamModelB.getResponse().get(position).getCountry().getCode();
            Glide.with(imageView.getContext()).load(url).into(imageView);
            name.setText(name1);
            country.setText(country1);
            code.setText(code1);
            GsonBuilder gsonBuilder=new GsonBuilder();
            String json=gsonBuilder.create().toJson(new TeamsBaAndB(name1,country1,code1,url));
            if(roomViewModel.checkExistence(json)!=0) {favorite.setImageResource(R.drawable.ic_baseline_favorite_24);}else favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
            favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(roomViewModel.checkExistence(json)==0) {
                        favorite.setImageResource(R.drawable.ic_baseline_favorite_24);
                        roomViewModel.addDataToDB(new DataModelRoom(json, Constants.TeamsBasketball));
                    }else {favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                        roomViewModel.deleteFromRoom(json);}
                }
            });
        }
    }

    public void setCountries(CountriesModelB list) {
        if (!listC.isEmpty()) {
            listC.clear();
        }
        listC.add(list);
        notifyDataSetChanged();
    }

    public void setLeagues(LeaguesModelB list) {
        if (!listL.isEmpty()) {
            listL.clear();
        }
        listL.add(list);
        notifyDataSetChanged();
    }

    public void setTeams(TeamModelB list) {
        if (!listT.isEmpty()) {
            listT.clear();
        }
        listT.add(list);
        notifyDataSetChanged();
    }

}
