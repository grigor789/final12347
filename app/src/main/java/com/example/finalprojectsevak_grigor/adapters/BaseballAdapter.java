package com.example.finalprojectsevak_grigor.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.finalprojectsevak_grigor.Constants;
import com.example.finalprojectsevak_grigor.R;
import com.example.finalprojectsevak_grigor.ViewModels.ViewModelForRoom;
import com.example.finalprojectsevak_grigor.dataBase.DataModelRoom;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.Countries.BaseBallCountries;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.Teams.BaseBallTeams;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.leagues.BaseBallLeagues;

import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.BasketballandBaseball.CountriesBaAndB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.BasketballandBaseball.LeaguesBaAndB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.BasketballandBaseball.TeamsBaAndB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.football.CountriesFootball;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.football.TeamsFootball;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

public class BaseballAdapter extends RecyclerView.Adapter {
    private int a;
    private static ArrayList<BaseBallCountries> listC = new ArrayList<>();
    private static ArrayList<BaseBallLeagues> listL = new ArrayList<>();
    private static ArrayList<BaseBallTeams> listT = new ArrayList<>();
    private static ViewModelForRoom roomViewModel;

    public BaseballAdapter(int a, ViewModelForRoom roomViewModel) {
        this.a = a;
        this.roomViewModel = roomViewModel;
    }

    @Override
    public int getItemViewType(int position) {
        switch (a) {
            case 0:
                return 0;
            case 1:
                return 1;
            case 2:
                return 2;
            default:
                return -1;
        }

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view;
        switch (viewType) {
            case 0:
                view = layoutInflater.inflate(R.layout.baseball_countres_cardview, parent, false);
                return new BaseballAdapter.CountriesHolder(view);
            case 1:
                view = layoutInflater.inflate(R.layout.baseball_teams_cardview, parent, false);
                return new BaseballAdapter.TeamsHolder(view);
            case 2:
                view = layoutInflater.inflate(R.layout.leaguescardview_baseball, parent, false);
                return new BaseballAdapter.LeaguesHolder(view);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case 0: {
               BaseballAdapter.CountriesHolder countriesHolder = (BaseballAdapter.CountriesHolder) holder;
                countriesHolder.bindC(position);
            }
            break;
            case 1: {
                BaseballAdapter.TeamsHolder teamsHolder = (BaseballAdapter.TeamsHolder) holder;
                teamsHolder.bindT(position);
            }
            break;
            case 2: {
                BaseballAdapter.LeaguesHolder leaguesHolder = (BaseballAdapter.LeaguesHolder) holder;
                leaguesHolder.bindL(position);

            }
            break;
        }


    }

    @Override
    public int getItemCount() {
        switch (a) {
            case 0: if(!listC.isEmpty()&&!listC.get(0).getResponse().isEmpty()){return listC.get(0).getResponse().size();}else return 0;
            case 1: if(!listT.isEmpty()&& !listT.get(0).getResponse().isEmpty()){return listT.get(0).getResponse().size();}else return 0;
            case 2:  if(!listL.isEmpty()&&!listL.get(0).getResponse().isEmpty()){return listL.get(0).getResponse().size();}else return 0;
            default:
                return 0;
        }
    }

    static class CountriesHolder extends RecyclerView.ViewHolder {
        private final AppCompatImageView imageView;
        private final MaterialTextView name;
        private final MaterialTextView code;
        private final AppCompatImageView favorite;

        public CountriesHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.cardviewCountryBaseballImage);
            name = itemView.findViewById(R.id.countryNameCardViewBaseball);
            code = itemView.findViewById(R.id.codeCountryBCardViewBaseball);
            favorite=itemView.findViewById(R.id.favoriteCountriesBaseball);
        }

        void bindC(int position) {
            BaseBallCountries baseBallCountries = listC.get(0);
            String url = baseBallCountries.getResponse().get(position).getFlag();
            String name1 = baseBallCountries.getResponse().get(position).getName();
            String code1 = baseBallCountries.getResponse().get(position).getCode();
            Glide.with(imageView.getContext()).load(url).into(imageView);
            name.setText(name1);
            code.setText(code1);
            GsonBuilder gsonBuilder=new GsonBuilder();
            String json=gsonBuilder.create().toJson(new CountriesBaAndB(name1,code1,url));
            if(roomViewModel.checkExistence(json)!=0) {favorite.setImageResource(R.drawable.ic_baseline_favorite_24);}else favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
            favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(roomViewModel.checkExistence(json)==0) {
                        favorite.setImageResource(R.drawable.ic_baseline_favorite_24);
                        roomViewModel.addDataToDB(new DataModelRoom(json, Constants.CountryBaseball));
                    }else {favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                        roomViewModel.deleteFromRoom(json);}
                }
            });

        }
    }

    static class LeaguesHolder extends RecyclerView.ViewHolder {
        private final AppCompatImageView imageView;
        private final MaterialTextView name;
        private final MaterialTextView country;
        private final MaterialTextView type;
        private final AppCompatImageView favorite;

        public LeaguesHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.cardviewLeaguesBaseballImage);
            name = itemView.findViewById(R.id.nameLeaguesCardViewBaseball);
            country = itemView.findViewById(R.id.countrylLeagueBaseballCardView);
            type = itemView.findViewById(R.id.cardViewLeagueBaseball);
            favorite=itemView.findViewById(R.id.favoriteLeaguesBaseball);
        }

        void bindL(int position) {
            BaseBallLeagues leaguesModelB = listL.get(0);
            String url = leaguesModelB.getResponse().get(position).getLogo();
            String name1 = leaguesModelB.getResponse().get(position).getName();
            String country1 = leaguesModelB.getResponse().get(position).getCountry().getName();
            String type1 = leaguesModelB.getResponse().get(position).getType();
            Glide.with(imageView.getContext()).load(url).into(imageView);
            name.setText(name1);
            country.setText(country1);
            type.setText(type1);
            GsonBuilder gsonBuilder=new GsonBuilder();
            String json=gsonBuilder.create().toJson(new LeaguesBaAndB(name1,country1,type1,url));
            if(roomViewModel.checkExistence(json)!=0) {favorite.setImageResource(R.drawable.ic_baseline_favorite_24);}else favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
            favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(roomViewModel.checkExistence(json)==0) {
                        favorite.setImageResource(R.drawable.ic_baseline_favorite_24);
                        roomViewModel.addDataToDB(new DataModelRoom(json, Constants.LeaguesBaseball));
                    }else {favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                        roomViewModel.deleteFromRoom(json);}
                }
            });
        }
    }

    static class TeamsHolder extends RecyclerView.ViewHolder {
        private final AppCompatImageView imageView;
        private final MaterialTextView name;
        private final MaterialTextView country;
        private final MaterialTextView code;
        private final AppCompatImageView favorite;

        public TeamsHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.cardviewTeamsBaseballImage);
            name = itemView.findViewById(R.id.nameTeamCardViewBaseball);
            country = itemView.findViewById(R.id.countryTeamBaseballCardView);
            code = itemView.findViewById(R.id.cardViewTeamBaseballCode);
        favorite=itemView.findViewById(R.id.favoriteTeamsBaseball);
        }

        void bindT(int position) {
            BaseBallTeams teamModelB = listT.get(0);
            String url = teamModelB.getResponse().get(position).getLogo();
            String name1 = teamModelB.getResponse().get(position).getName();
            String country1 = teamModelB.getResponse().get(position).getCountry().getName();
            String code1 = teamModelB.getResponse().get(position).getCountry().getCode();
            Glide.with(imageView.getContext()).load(url).into(imageView);
            name.setText(name1);
            country.setText(country1);
            code.setText(code1);
            GsonBuilder gsonBuilder=new GsonBuilder();
            String json=gsonBuilder.create().toJson(new TeamsBaAndB(name1,country1,code1,url));
            if(roomViewModel.checkExistence(json)!=0) {favorite.setImageResource(R.drawable.ic_baseline_favorite_24);}else favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
            favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(roomViewModel.checkExistence(json)==0) {
                        favorite.setImageResource(R.drawable.ic_baseline_favorite_24);
                        roomViewModel.addDataToDB(new DataModelRoom(json, Constants.TeamsBaseball));
                    }else {favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                        roomViewModel.deleteFromRoom(json);}
                }
            });
        }
    }

    public void setCountries(BaseBallCountries list) {
        if(!listC.isEmpty()){listC.clear();}
        listC.add(list);
        notifyDataSetChanged();
    }

    public void setLeagues(BaseBallLeagues list) {
        if(!listL.isEmpty()){listL.clear();}
        listL.add( list);
        notifyDataSetChanged();
    }

    public void setTeams(BaseBallTeams list) {
        if(!listT.isEmpty()){listT.clear();}
        listT.add( list);
        notifyDataSetChanged();
    }

}
