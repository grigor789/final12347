package com.example.finalprojectsevak_grigor.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.finalprojectsevak_grigor.Constants;
import com.example.finalprojectsevak_grigor.R;
import com.example.finalprojectsevak_grigor.dataBase.DataModelRoom;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.Countries.BaseBallCountries;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.BasketballandBaseball.CountriesBaAndB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.BasketballandBaseball.LeaguesBaAndB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.BasketballandBaseball.TeamsBaAndB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.football.CountriesFootball;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.football.PlayersFootball;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.football.TeamsFootball;
import com.github.twocoffeesoneteam.glidetovectoryou.GlideApp;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.converter.gson.GsonConverterFactory;

public class FavoriteAdapter extends RecyclerView.Adapter {
    private List<DataModelRoom> list = new ArrayList<>();


    @Override
    public int getItemViewType(int position) {
        switch (list.get(position).getType()) {
            case Constants.CountryBaseball:
                return 0;

            case Constants.TeamsBaseball:
                return 1;
            case Constants.LeaguesBaseball:
                return 2;
            case Constants.CountryBasketball:
                return 3;
            case Constants.TeamsBasketball:
                return 4;
            case Constants.LeaguesBasketball:
                return 5;
            case Constants.CountryFootball:
                return 6;
            case Constants.TeamsFootball:
                return 7;
            case Constants.PlayerFootball:
                return 8;
        }


        return super.getItemViewType(position);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view;
        switch (viewType) {
            case 0:
                view = layoutInflater.inflate(R.layout.baseball_countres_cardview, parent, false);
                return new BaseballAdapter.CountriesHolder(view);
            case 1:
                view = layoutInflater.inflate(R.layout.baseball_teams_cardview, parent, false);
                return new BaseballAdapter.TeamsHolder(view);
            case 2:
                view = layoutInflater.inflate(R.layout.leaguescardview_baseball, parent, false);
                return new BaseballAdapter.LeaguesHolder(view);
            case 3:
                view = layoutInflater.inflate(R.layout.countriescardview_b, parent, false);
                return new BasketballAdapter.CountriesHolder(view);
            case 4:
                view = layoutInflater.inflate(R.layout.teamscardview_b, parent, false);
                return new BasketballAdapter.TeamsHolder(view);
            case 5:
                view = layoutInflater.inflate(R.layout.leaguescardview_b, parent, false);
                return new BasketballAdapter.LeaguesHolder(view);
            case 6:
                view = layoutInflater.inflate(R.layout.countries_f_cardview, parent, false);
                return new FootballAdapter.CountriesHolder(view);
            case 7:
                view = layoutInflater.inflate(R.layout.teamscardviewf, parent, false);
                return new FootballAdapter.TeamsHolder(view);
            case 8:
                view = layoutInflater.inflate(R.layout.playerscardviewf, parent, false);
                return new FootballAdapter.PlayersHolder(view);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case 0: {
                BaseballAdapter.CountriesHolder countriesHolder = (BaseballAdapter.CountriesHolder) holder;
                setCountryBa(countriesHolder, position);

            }
            break;
            case 1: {
                BaseballAdapter.TeamsHolder teamsHolder = (BaseballAdapter.TeamsHolder) holder;
                setTeamsBa(teamsHolder, position);
            }
            break;
            case 2: {
                BaseballAdapter.LeaguesHolder leaguesHolder = (BaseballAdapter.LeaguesHolder) holder;
                setLeaguesBa(leaguesHolder, position);

            }
            break;
            case 3: {
                BasketballAdapter.CountriesHolder countriesHolder = (BasketballAdapter.CountriesHolder) holder;
                setCountriesB(countriesHolder, position);
            }
            break;
            case 4: {
                BasketballAdapter.TeamsHolder teamsHolder = (BasketballAdapter.TeamsHolder) holder;
                setTeamsB(teamsHolder, position);
            }
            break;
            case 5: {
                BasketballAdapter.LeaguesHolder leaguesHolder = (BasketballAdapter.LeaguesHolder) holder;
                setLeaguesB(leaguesHolder, position);

            }
            break;
            case 6: {
                FootballAdapter.CountriesHolder countriesHolder = (FootballAdapter.CountriesHolder) holder;
                setCountriesF(countriesHolder, position);
            }
            break;
            case 7: {
                FootballAdapter.TeamsHolder teamsHolder = (FootballAdapter.TeamsHolder) holder;
                setTeamsF(teamsHolder, position);
            }
            break;
            case 8: {
                FootballAdapter.PlayersHolder playersHolder = (FootballAdapter.PlayersHolder) holder;
                setPlayersF(playersHolder, position);

            }
            break;
        }

    }


    @SuppressLint("CheckResult")
    private void setCountryBa(BaseballAdapter.CountriesHolder countriesHolder, int position) {
        AppCompatImageView imageView = countriesHolder.itemView
                .findViewById(R.id.cardviewCountryBaseballImage);
        MaterialTextView name = countriesHolder.itemView.findViewById(R.id.countryNameCardViewBaseball);
        MaterialTextView code = countriesHolder.itemView.findViewById(R.id.codeCountryBCardViewBaseball);
        AppCompatImageView favorite = countriesHolder.itemView.findViewById(R.id.favoriteCountriesBaseball);
        favorite.setVisibility(View.GONE);
        String json = list.get(position).getJson();
        CountriesBaAndB baseBallCountries = new Gson().fromJson(json, CountriesBaAndB.class);
        Glide.with(imageView.getContext()).load(baseBallCountries.getUrl()).into(imageView);
        name.setText(baseBallCountries.getName());
        code.setText(baseBallCountries.getCode());
    }

    @SuppressLint("CheckResult")
    private void setLeaguesBa(BaseballAdapter.LeaguesHolder leaguesHolder, int position) {
        AppCompatImageView imageView = leaguesHolder.itemView.findViewById(R.id.cardviewLeaguesBaseballImage);
        MaterialTextView name = leaguesHolder.itemView.findViewById(R.id.nameLeaguesCardViewBaseball);
        MaterialTextView country = leaguesHolder.itemView.findViewById(R.id.countrylLeagueBaseballCardView);
        MaterialTextView type = leaguesHolder.itemView.findViewById(R.id.cardViewLeagueBaseball);
        AppCompatImageView favorite = leaguesHolder.itemView.findViewById(R.id.favoriteLeaguesBaseball);
        favorite.setVisibility(View.GONE);
        String json = list.get(position).getJson();
        LeaguesBaAndB leaguesBaAndB = new Gson().fromJson(json, LeaguesBaAndB.class);
        Glide.with(imageView.getContext()).load(leaguesBaAndB.getUrl()).into(imageView);
        name.setText(leaguesBaAndB.getName());
        country.setText(leaguesBaAndB.getCountry());
        type.setText(leaguesBaAndB.getType());

    }

    @SuppressLint("CheckResult")
    private void setTeamsBa(BaseballAdapter.TeamsHolder teamsHolder, int position) {
        AppCompatImageView imageView = teamsHolder.itemView.findViewById(R.id.cardviewTeamsBaseballImage);
        MaterialTextView name = teamsHolder.itemView.findViewById(R.id.nameTeamCardViewBaseball);
        MaterialTextView country = teamsHolder.itemView.findViewById(R.id.countryTeamBaseballCardView);
        MaterialTextView code = teamsHolder.itemView.findViewById(R.id.cardViewTeamBaseballCode);
        AppCompatImageView favorite = teamsHolder.itemView.findViewById(R.id.favoriteTeamsBaseball);
        favorite.setVisibility(View.GONE);
        String json = list.get(position).getJson();
        TeamsBaAndB teamsBaAndB = new Gson().fromJson(json, TeamsBaAndB.class);
        Glide.with(imageView.getContext()).load(teamsBaAndB.getUrl()).into(imageView);
        name.setText(teamsBaAndB.getName());
        country.setText(teamsBaAndB.getCountry());
        code.setText(teamsBaAndB.getCode());

    }

    @SuppressLint("CheckResult")
    private void setCountriesB(BasketballAdapter.CountriesHolder countriesHolder, int position) {
        AppCompatImageView imageView = countriesHolder.itemView.findViewById(R.id.cardviewCountryBImage);
        MaterialTextView name = countriesHolder.itemView.findViewById(R.id.countryNameCardViewB);
        MaterialTextView code = countriesHolder.itemView.findViewById(R.id.codeCountryBCardView);
       AppCompatImageView favorite = countriesHolder.itemView.findViewById(R.id.favoriteCountriesB);
        favorite.setVisibility(View.GONE);
        String json = list.get(position).getJson();
        CountriesBaAndB baseBallCountries = new Gson().fromJson(json, CountriesBaAndB.class);
        Glide.with(imageView.getContext()).load(baseBallCountries.getUrl()).into(imageView);
        name.setText(baseBallCountries.getName());
        code.setText(baseBallCountries.getCode());
    }

    @SuppressLint("CheckResult")
    private void setTeamsB(BasketballAdapter.TeamsHolder teamsHolder, int position) {
      AppCompatImageView  imageView =teamsHolder. itemView.findViewById(R.id.cardviewTeamBImage);
       MaterialTextView name = teamsHolder.itemView.findViewById(R.id.nameTeamCardViewB);
       MaterialTextView country = teamsHolder.itemView.findViewById(R.id.countryTeamBCardView);
      MaterialTextView  code = teamsHolder.itemView.findViewById(R.id.cardViewTeamBCode);
      AppCompatImageView  favorite = teamsHolder.itemView.findViewById(R.id.favoriteTeamB);
        favorite.setVisibility(View.GONE);
        String json = list.get(position).getJson();
        TeamsBaAndB teamsBaAndB = new Gson().fromJson(json, TeamsBaAndB.class);
        Glide.with(imageView.getContext()).load(teamsBaAndB.getUrl()).into(imageView);
        name.setText(teamsBaAndB.getName());
        country.setText(teamsBaAndB.getCountry());
        code.setText(teamsBaAndB.getCode());
    }

    @SuppressLint("CheckResult")
    private void setLeaguesB(BasketballAdapter.LeaguesHolder leaguesHolder, int position) {
       AppCompatImageView imageView = leaguesHolder.itemView.findViewById(R.id.cardviewLeaguesBImage);
        MaterialTextView name = leaguesHolder.itemView.findViewById(R.id.nameLeaguesCardViewB);
        MaterialTextView country = leaguesHolder.itemView.findViewById(R.id.countrylLeagueBCardView);
        MaterialTextView type = leaguesHolder.itemView.findViewById(R.id.cardViewTypeLeagueB);
        AppCompatImageView favorite = leaguesHolder.itemView.findViewById(R.id.favoriteLeaguesB);
        favorite.setVisibility(View.GONE);
        String json = list.get(position).getJson();
        LeaguesBaAndB leaguesBaAndB = new Gson().fromJson(json, LeaguesBaAndB.class);
        Glide.with(imageView.getContext()).load(leaguesBaAndB.getUrl()).into(imageView);
        name.setText(leaguesBaAndB.getName());
        country.setText(leaguesBaAndB.getCountry());
        type.setText(leaguesBaAndB.getType());
    }

    @SuppressLint("CheckResult")
    private void setCountriesF(FootballAdapter.CountriesHolder countriesHolder, int position) {
        AppCompatImageView imageView =countriesHolder. itemView.findViewById(R.id.cardviewCountryFImage);
        MaterialTextView name = countriesHolder.itemView.findViewById(R.id.countryNameCardViewF);
        MaterialTextView capital = countriesHolder.itemView.findViewById(R.id.capitalCountryFCardView);
        MaterialTextView subregion = countriesHolder.itemView.findViewById(R.id.cardViewCountryFRegion);
        AppCompatImageView favorite=countriesHolder.itemView.findViewById(R.id.favoriteCountriesF);
        String json = list.get(position).getJson();
        CountriesFootball countriesFootball = new Gson().fromJson(json, CountriesFootball.class);
        GlideApp.with(countriesHolder.itemView.getContext()).load(countriesFootball.getUrl()).into(imageView);
        name.setText(countriesFootball.getName());
        capital.setText(countriesFootball.getCapital());
        subregion.setText(countriesFootball.getSubRegion());
        favorite.setVisibility(View.GONE);
    }

    @SuppressLint("CheckResult")
    private void setTeamsF(FootballAdapter.TeamsHolder teamsHolder, int position) {
       AppCompatImageView imageView = teamsHolder.itemView.findViewById(R.id.cardviewTeamFImage);
        MaterialTextView name = teamsHolder.itemView.findViewById(R.id.nameTeamCardViewF);
        MaterialTextView shortCode = teamsHolder.itemView.findViewById(R.id.shortcodeTeamFCardView);
        MaterialTextView founded = teamsHolder.itemView.findViewById(R.id.cardViewTeamFFounded);
        AppCompatImageView favorite=teamsHolder.itemView.findViewById(R.id.favoriteTeamF);
        String json = list.get(position).getJson();
        TeamsFootball teamsFootball = new Gson().fromJson(json, TeamsFootball.class);
        Glide.with(imageView.getContext()).load(teamsFootball.getUrl()).into(imageView);
        name.setText(teamsFootball.getName());
        shortCode.setText(teamsFootball.getShortCode());
        founded.setText(teamsFootball.getFounded());
        favorite.setVisibility(View.GONE);
    }

    @SuppressLint("CheckResult")
    private void setPlayersF(FootballAdapter.PlayersHolder playersHolder, int position) {
      AppCompatImageView  imageView = playersHolder.itemView.findViewById(R.id.cardviewPlayerFImage);
        MaterialTextView name = playersHolder.itemView.findViewById(R.id.namePlayerCardViewF);
        MaterialTextView weight = playersHolder.itemView.findViewById(R.id.playerFCardView);
       MaterialTextView height = playersHolder.itemView.findViewById(R.id.cardPlayerViewFHeight);
       MaterialTextView foot = playersHolder.itemView.findViewById(R.id.cardPlayerViewFFoot);
        AppCompatImageView favorite=playersHolder.itemView.findViewById(R.id.favoriteCountriesPlayerF);
        favorite.setVisibility(View.GONE);
        String json = list.get(position).getJson();
        PlayersFootball playersFootball= new Gson().fromJson(json, PlayersFootball.class);
        Glide.with(imageView.getContext()).load(playersFootball.getUrl()).into(imageView);
        name.setText(playersFootball.getName());
        weight.setText(playersFootball.getWeight());
        height.setText(playersFootball.getHeight());
        foot.setText(playersFootball.getFoot());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setData(List<DataModelRoom> arrayList) {
        this.list = arrayList;
        notifyDataSetChanged();
    }
}
