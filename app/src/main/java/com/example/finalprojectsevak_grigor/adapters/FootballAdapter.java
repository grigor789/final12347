package com.example.finalprojectsevak_grigor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.finalprojectsevak_grigor.Constants;
import com.example.finalprojectsevak_grigor.R;
import com.example.finalprojectsevak_grigor.ViewModels.ViewModelForRoom;
import com.example.finalprojectsevak_grigor.dataBase.DataModelRoom;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.LeagueB.LeaguesModelB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.countries.CountriesModelB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.teams.TeamModelB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Football.CountryModel.CountriesModel;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Football.PlayerModel.PlayerModel;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Football.TeamModel.TeamModel;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.football.CountriesFootball;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.football.PlayersFootball;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.modelsforfav.football.TeamsFootball;
import com.github.twocoffeesoneteam.glidetovectoryou.GlideApp;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

public class FootballAdapter extends RecyclerView.Adapter {
    private int a;
    private static ArrayList<CountriesModel> listC = new ArrayList<>();
    private static ArrayList<PlayerModel> listP = new ArrayList<>();
    private static ArrayList<TeamModel> listT = new ArrayList<>();
    private static ViewModelForRoom roomViewModel;

    public FootballAdapter(int a,ViewModelForRoom roomViewModel) {
        this.a = a;
        this.roomViewModel=roomViewModel;
    }


    @Override
    public int getItemViewType(int position) {
        switch (a) {
            case 0:
                return 0;
            case 1:
                return 1;
            case 2:
                return 2;
            default:
                return -1;
        }

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view;
        switch (viewType) {
            case 0:
                view = layoutInflater.inflate(R.layout.countries_f_cardview, parent, false);
                return new FootballAdapter.CountriesHolder(view);
            case 1:
                view = layoutInflater.inflate(R.layout.teamscardviewf, parent, false);
                return new FootballAdapter.TeamsHolder(view);
            case 2:
                view = layoutInflater.inflate(R.layout.playerscardviewf, parent, false);
                return new FootballAdapter.PlayersHolder(view);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case 0: {
                FootballAdapter.CountriesHolder countriesHolder = (FootballAdapter.CountriesHolder) holder;
                countriesHolder.bindC(position);
            }
            break;
            case 1: {
                FootballAdapter.TeamsHolder teamsHolder = (FootballAdapter.TeamsHolder) holder;
                teamsHolder.bindT(position);
            }
            break;
            case 2: {
                FootballAdapter.PlayersHolder playersHolder = (FootballAdapter.PlayersHolder) holder;
                playersHolder.bindP(position);

            }
            break;
        }


    }

    @Override
    public int getItemCount() {
        switch (a) {
            case 0:
                if (!listC.isEmpty() && !listC.get(0).getData().isEmpty()) {
                    return listC.get(0).getData().size();
                } else return 0;
            case 1:
                if (!listT.isEmpty() && !listT.get(0).getData().isEmpty()) {
                    return listT.get(0).getData().size();
                } else return 0;
            case 2:
                if (!listP.isEmpty() && !listP.get(0).getData().isEmpty()) {
                    return listP.get(0).getData().size();
                } else return 0;
            default:
                return 0;
        }

    }

    static class CountriesHolder extends RecyclerView.ViewHolder {
        private final AppCompatImageView imageView;
        private final MaterialTextView name;
        private final MaterialTextView capital;
        private final MaterialTextView subregion;
        private final AppCompatImageView favorite;

        public CountriesHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.cardviewCountryFImage);
            name = itemView.findViewById(R.id.countryNameCardViewF);
            capital = itemView.findViewById(R.id.capitalCountryFCardView);
            subregion = itemView.findViewById(R.id.cardViewCountryFRegion);
            favorite=itemView.findViewById(R.id.favoriteCountriesF);

        }

        void bindC(int position) {
            CountriesModel countriesModel = listC.get(0);
            String url = countriesModel.getData().get(position).getFlagSVG();
            String name1 = countriesModel.getData().get(position).getName();
            String capital1 = countriesModel.getData().get(position).getCapital();
            String subRegion = countriesModel.getData().get(position).getRegion();
            Glide.with(imageView.getContext()).load(url).into(imageView);
            name.setText(name1);
            capital.setText(capital1);
            subregion.setText(subRegion);
            GsonBuilder gsonBuilder=new GsonBuilder();
            String json=gsonBuilder.create().toJson(new CountriesFootball(name1,capital1,subRegion,url));
            if(roomViewModel.checkExistence(json)!=0) {favorite.setImageResource(R.drawable.ic_baseline_favorite_24);}else favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
            favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(roomViewModel.checkExistence(json)==0) {
                        favorite.setImageResource(R.drawable.ic_baseline_favorite_24);
                        roomViewModel.addDataToDB(new DataModelRoom(json, Constants.CountryFootball));
                    }else {favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                        roomViewModel.deleteFromRoom(json);}
                }
            });


        }
    }

    static class PlayersHolder extends RecyclerView.ViewHolder {
        private final AppCompatImageView imageView;
        private final MaterialTextView name;
        private final MaterialTextView weight;
        private final MaterialTextView height;
        private final MaterialTextView foot;
        private final AppCompatImageView favorite;

        public PlayersHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.cardviewPlayerFImage);
            name = itemView.findViewById(R.id.namePlayerCardViewF);
            weight = itemView.findViewById(R.id.playerFCardView);
            height = itemView.findViewById(R.id.cardPlayerViewFHeight);
            foot = itemView.findViewById(R.id.cardPlayerViewFFoot);
            favorite=itemView.findViewById(R.id.favoriteCountriesPlayerF);


        }

        void bindP(int position) {
            PlayerModel playerModel = listP.get(0);
            String url = playerModel.getData().get(position).getPhotoURL();
            String name1 = playerModel.getData().get(position).getFullName();
            String weight1 = "-";
            if (playerModel.getData().get(position).getWeight() != null) {
                weight1 = String.valueOf(playerModel.getData().get(position).getWeight());
            }
            String height1 = "-";
            if (playerModel.getData().get(position).getHeight() != null) {
                height1 = String.valueOf(playerModel.getData().get(position).getHeight());
            }
            String foot1 = playerModel.getData().get(position).getFoot();
            Glide.with(imageView.getContext()).load(url).into(imageView);
            name.setText(name1);
            weight.setText(weight1);
            height.setText(height1);
            foot.setText(foot1);
            String finalHeight = height1;
            String finalWeight = weight1;
            GsonBuilder gsonBuilder=new GsonBuilder();
            String json=gsonBuilder.create().toJson(new PlayersFootball(name1,weight1,height1,foot1,url));
            if(roomViewModel.checkExistence(json)!=0) {favorite.setImageResource(R.drawable.ic_baseline_favorite_24);}else favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
            favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(roomViewModel.checkExistence(json)==0) {
                        favorite.setImageResource(R.drawable.ic_baseline_favorite_24);
                        roomViewModel.addDataToDB(new DataModelRoom(json, Constants.PlayerFootball));
                    }else {favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                        roomViewModel.deleteFromRoom(json);}
                }
            });
        }
    }

    static class TeamsHolder extends RecyclerView.ViewHolder {
        private final AppCompatImageView imageView;
        private final MaterialTextView name;
        private final MaterialTextView shortCode;
        private final MaterialTextView founded;
        private final AppCompatImageView favorite;

        public TeamsHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.cardviewTeamFImage);
            name = itemView.findViewById(R.id.nameTeamCardViewF);
            shortCode = itemView.findViewById(R.id.shortcodeTeamFCardView);
            founded = itemView.findViewById(R.id.cardViewTeamFFounded);
            favorite=itemView.findViewById(R.id.favoriteTeamF);
        }

        void bindT(int position) {
            TeamModel teamModel = listT.get(0);
            String url = teamModel.getData().get(position).getLogoPath();
            String name1 = teamModel.getData().get(position).getName();
            String code1 = teamModel.getData().get(position).getShortCode();
            String founded1 = teamModel.getData().get(position).getFounded().toString();
            Glide.with(imageView.getContext()).load(url).into(imageView);
            name.setText(name1);
            shortCode.setText(code1);
            founded.setText(founded1);
            GsonBuilder gsonBuilder=new GsonBuilder();
            String json=gsonBuilder.create().toJson(new TeamsFootball(name1,code1,founded1,url));
            if(roomViewModel.checkExistence(json)!=0) {favorite.setImageResource(R.drawable.ic_baseline_favorite_24);}else favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
            favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(roomViewModel.checkExistence(json)==0) {
                        favorite.setImageResource(R.drawable.ic_baseline_favorite_24);
                        roomViewModel.addDataToDB(new DataModelRoom(json, Constants.TeamsFootball));
                    }else {favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                        roomViewModel.deleteFromRoom(json);}
                }
            });
        }
    }

    public void setCountries(CountriesModel list) {
        if (!listC.isEmpty()) {
            listC.clear();
        }
        listC.add(list);
        notifyDataSetChanged();
    }

    public void setPlayers(PlayerModel list) {
        if (!listP.isEmpty()) {
            listP.clear();
        }
        listP.add(list);
        notifyDataSetChanged();
    }

    public void setTeams(TeamModel list) {
        if (!listT.isEmpty()) {
            listT.clear();
        }
        listT.add(list);
        notifyDataSetChanged();
    }
}
