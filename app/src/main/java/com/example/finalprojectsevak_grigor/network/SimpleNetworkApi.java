package com.example.finalprojectsevak_grigor.network;


import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.Countries.BaseBallCountries;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.Teams.BaseBallTeams;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.leagues.BaseBallLeagues;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.LeagueB.LeaguesModelB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.countries.CountriesModelB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.teams.TeamModelB;

import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Football.CountryModel.CountriesModel;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Football.PlayerModel.PlayerModel;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Football.TeamModel.TeamModel;


import io.reactivex.rxjava3.core.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface SimpleNetworkApi {
    /*Football*/
    @GET("countries")
    Observable<Response<CountriesModel>> getCountriesList(@Header("x-rapidapi-key") String header1);

    @GET("teams")
    Observable<Response<TeamModel>> getTeams(@Header("Authorization") String token);

    @GET("players")
    Observable<Response<PlayerModel>> getPlayer(@Header("x-rapidapi-key") String header1);

    /*****/
    /*BasketBall*/
    @GET("teams")
    Observable<Response<TeamModelB>> getTeamsB(@Header("x-rapidapi-key") String header1, @Header("x-rapidapi-host") String header2,
                                               @Query("league") String a, @Query("season") String b);

    @GET("countries")
    Observable<Response<CountriesModelB>> getCountriesB(@Header("x-rapidapi-key") String header1, @Header("x-rapidapi-host") String header2);

    @GET("leagues")
    Observable<Response<LeaguesModelB>> getLeagues(@Header("x-rapidapi-key") String header1);

    /*****/
    /*Baseball*/
    @GET("countries")
    Observable<Response<BaseBallCountries>> getCountriesBaseBall(@Header("x-rapidapi-key") String header1);

    @GET("leagues/")
    Observable<Response<BaseBallLeagues>> getLeaguesBaseball(@Header("x-rapidapi-key") String header1);

    @GET("teams")
    Observable<Response<BaseBallTeams>> getTeamsBaseball(@Header("x-rapidapi-key") String header1,
                                                         @Query("league") String a, @Query("season") String b);

    }


