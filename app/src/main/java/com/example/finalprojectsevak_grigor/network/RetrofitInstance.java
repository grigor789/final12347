package com.example.finalprojectsevak_grigor.network;

import com.example.finalprojectsevak_grigor.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {

    private Retrofit retrofit;


    public Retrofit getInstance(String url) {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.level(HttpLoggingInterceptor.Level.BASIC);

        OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(loggingInterceptor).build();
        if (retrofit == null) {
            retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava3CallAdapterFactory.create()).client(okHttpClient)
                    .baseUrl(url)
                    .build();

        }
        return retrofit;
    }

    private SimpleNetworkApi simpleNetworkApi;

    public SimpleNetworkApi getApi(String url) {
        if (simpleNetworkApi == null) {
            simpleNetworkApi = new RetrofitInstance().getInstance(url).create(SimpleNetworkApi.class);

        }
        return simpleNetworkApi;
    }





}