package com.example.finalprojectsevak_grigor.ViewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

public class MainViewModel extends ViewModel {

    private MutableLiveData<String> date;

    public LiveData<String> getDate() {
        if (date == null) {
            date = new MutableLiveData<String>();
        }
        return date;
    }

    public void setDate(String date) {
        if (date != null) {
            this.date.setValue(date);
        }
    }

    private MutableLiveData<String> logged_out;

    public MutableLiveData<String> getLoggedOut() {
        if (logged_out == null) {
            logged_out = new MutableLiveData<>();
        }
        return logged_out;
    }

    public void setLogged_out(String date) {
        if (date != null) {
            this.getLoggedOut().setValue(date);
        }
    }
    private MutableLiveData<String> setVolumeView;

    public MutableLiveData<String> getVolumeView() {
        if (setVolumeView == null) {
            setVolumeView = new MutableLiveData<>();
        }
        return setVolumeView;
    }

    public void serVolumeView(String volume) {
        if (volume != null) {
            getVolumeView().setValue(volume);
        }
    }

    /*****************countriesF***********************/

    private MutableLiveData<String> setCountriesFClicked;

    public MutableLiveData<String> getCountriesFClicked() {
        if (setCountriesFClicked == null) {
            setCountriesFClicked = new MutableLiveData<>();
        }
        return setCountriesFClicked;
    }

    public void setCountriesFClicked(String path) {
        if (path != null) {
            getCountriesFClicked().setValue(path);
        }
    }
    /*****************PlayersF***********************/

    private MutableLiveData<String> setPlayersFClicked;

    public MutableLiveData<String> getPlayersFClicked() {
        if (setPlayersFClicked == null) {
            setPlayersFClicked = new MutableLiveData<>();
        }
        return setPlayersFClicked;
    }

    public void setPlayersFClicked(String path) {
        if (path != null) {
            getPlayersFClicked().setValue(path);
        }
    }

/**********************TeamsF*************************/
private MutableLiveData<String> setTeamsFClicked;

    public MutableLiveData<String> getTeamsFClicked() {
        if (setTeamsFClicked == null) {
            setTeamsFClicked = new MutableLiveData<>();
        }
        return setTeamsFClicked;
    }

    public void setTeamsFClicked(String path) {
        if (path != null) {
            getTeamsFClicked().setValue(path);
        }
    }

    /*****************countriesB***********************/

    private MutableLiveData<String> setCountriesBClicked;

    public MutableLiveData<String> getCountriesBClicked() {
        if (setCountriesBClicked == null) {
            setCountriesBClicked = new MutableLiveData<>();
        }
        return setCountriesBClicked;
    }

    public void setCountriesBClicked(String path) {
        if (path != null) {
            getCountriesBClicked().setValue(path);
        }
    }
    /*****************leaguesB***********************/

    private MutableLiveData<String> setLeaguesBClicked;

    public MutableLiveData<String> getLeaguesBClicked() {
        if (setLeaguesBClicked == null) {
            setLeaguesBClicked = new MutableLiveData<>();
        }
        return setLeaguesBClicked;
    }

    public void setLeaguesBClicked(String path) {
        if (path != null) {
            getLeaguesBClicked().setValue(path);
        }
    }

    /**********************TeamsB*************************/
    private MutableLiveData<String> setTeamsBClicked;

    public MutableLiveData<String> getTeamsBClicked() {
        if (setTeamsBClicked == null) {
            setTeamsBClicked = new MutableLiveData<>();
        }
        return setTeamsBClicked;
    }

    public void setTeamsBClicked(String path) {
        if (path != null) {
            getTeamsBClicked().setValue(path);
        }
    }

    /*****************countriesBa***********************/

    private MutableLiveData<String> setCountriesBaClicked;

    public MutableLiveData<String> getCountriesBaClicked() {
        if (setCountriesBaClicked == null) {
            setCountriesBaClicked = new MutableLiveData<>();
        }
        return setCountriesBaClicked;
    }

    public void setCountriesBaClicked(String path) {
        if (path != null) {
            getCountriesBaClicked().setValue(path);
        }
    }
    /*****************leaguesBa***********************/

    private MutableLiveData<String> setLeaguesBaClicked;

    public MutableLiveData<String> getLeaguesBaClicked() {
        if (setLeaguesBaClicked == null) {
            setLeaguesBaClicked = new MutableLiveData<>();
        }
        return setLeaguesBaClicked;
    }

    public void setLeaguesBaClicked(String path) {
        if (path != null) {
            getLeaguesBaClicked().setValue(path);
        }
    }

    /**********************TeamsBa*************************/
    private MutableLiveData<String> setTeamsBaClicked;

    public MutableLiveData<String> getTeamsBaClicked() {
        if (setTeamsBaClicked == null) {
            setTeamsBaClicked = new MutableLiveData<>();
        }
        return setTeamsBaClicked;
    }

    public void setTeamsBaClicked(String path) {
        if (path != null) {
            getTeamsBaClicked().setValue(path);
        }
    }

}
