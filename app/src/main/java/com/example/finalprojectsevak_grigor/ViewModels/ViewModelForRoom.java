package com.example.finalprojectsevak_grigor.ViewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.finalprojectsevak_grigor.dataBase.DataDao;
import com.example.finalprojectsevak_grigor.dataBase.DataFinalDatabase;
import com.example.finalprojectsevak_grigor.dataBase.DataModelRoom;
import com.example.finalprojectsevak_grigor.repository.Repository;

import java.util.List;

public class ViewModelForRoom extends AndroidViewModel {
    private final Repository repository;
    public LiveData<List<DataModelRoom>> readData;

    public ViewModelForRoom(@NonNull Application application) {
        super(application);
        DataFinalDatabase db = DataFinalDatabase.getInstance(application);
        DataDao dataDao = db.dao();
        repository = new Repository(dataDao);
        readData = repository.readDataFromRoom();
    }
    public void addDataToDB(DataModelRoom dataModelRoom){
        repository.addDataToRoom(dataModelRoom);
    }
    public void updateDataInDB(DataModelRoom dataModelRoom){
        repository.updateDataInRoom(dataModelRoom);
    }
    public void deleteFromRoom(String json){
        repository.deleteDataFromRoom(json);
    }
    public void deleteALLFromRoom(){
        repository.deleteAll();
    }
    public int checkExistence(String json){
        return repository.checkExistence(json);
    }

}
