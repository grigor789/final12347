package com.example.finalprojectsevak_grigor.ViewModels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.Countries.BaseBallCountries;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.Teams.BaseBallTeams;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.BaseBall.leagues.BaseBallLeagues;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.LeagueB.LeaguesModelB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.countries.CountriesModelB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Basketball.teams.TeamModelB;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Football.CountryModel.CountriesModel;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Football.PlayerModel.PlayerModel;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.models.Football.TeamModel.TeamModel;
import com.example.finalprojectsevak_grigor.repository.Repository;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Response;

public class NetworkViewModel extends ViewModel {
    Repository repository = new Repository();

    /*Football*/
    public Observable<Response<CountriesModel>> getFootballCountries() {

        return repository.getCountries().subscribeOn(Schedulers.newThread());
    }

    public Observable<Response<TeamModel>> getFootballTeams() {
        return repository.getTeams().subscribeOn(Schedulers.newThread());
    }

    public Observable<Response<PlayerModel>> getFootballPlayer() {
        return repository.getPlayers().subscribeOn(Schedulers.newThread());
    }

    /*BasketBall*/

    public Observable<Response<TeamModelB>> getTeamsB() {
        return repository.getTeamsB().subscribeOn(Schedulers.newThread());
    }

    public Observable<Response<CountriesModelB>> getCountriesB() {
        return repository.getCountriesB().subscribeOn(Schedulers.newThread());
    }

    public Observable<Response<LeaguesModelB>> getLeaguesB() {
        return repository.getLeaguesB().subscribeOn(Schedulers.newThread());
    }

    /********************/

    /*Baseball*/
    public Observable<Response<BaseBallTeams>> getBaseballTeams() {
        return repository.getBaseballTeams().subscribeOn(Schedulers.newThread());
    }

    public Observable<Response<BaseBallLeagues>> getLeaguesBaseball() {
        return repository.getLeaguesBaseball().subscribeOn(Schedulers.newThread());
    }

    public Observable<Response<BaseBallCountries>> getCountriesBaseBall() {
        return repository.getCountriesBaseBall().subscribeOn(Schedulers.newThread());
    }

}
