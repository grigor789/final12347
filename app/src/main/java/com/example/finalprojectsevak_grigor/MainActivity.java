package com.example.finalprojectsevak_grigor;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.finalprojectsevak_grigor.ViewModels.MainViewModel;
import com.example.finalprojectsevak_grigor.ViewModels.NetworkViewModel;
import com.example.finalprojectsevak_grigor.ViewModels.ViewModelForRoom;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.inside.HomeFragment;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.outside.SplashFragment;
import com.example.finalprojectsevak_grigor.fragmentsAndUtilities.service.MyService;

import java.util.Objects;

import io.reactivex.rxjava3.disposables.CompositeDisposable;

public class MainActivity extends AppCompatActivity implements HomeFragment.OnBackPressed {
    private DrawerLayout drawerLayout;
    private Intent intent;
    private MainViewModel viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewModel=new ViewModelProvider(this).get(MainViewModel.class);
        intent = new Intent(getApplicationContext(), MyService.class);
        openSplash();

    }

    private void openSplash() {
        SplashFragment splashFragment = new SplashFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container1, splashFragment).commit();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
        } else {
            getSupportFragmentManager().popBackStack();
        }

    }

    @Override
    public void onBackPressed(DrawerLayout drawerLayout) {
        this.drawerLayout = drawerLayout;

    }

    @Override
    protected void onStart() {
        super.onStart();
         String[] a = new String[1];
        viewModel.getVolumeView().observe(this, new Observer<String>() {

            @Override
            public void onChanged(String s) {
               a[0] =s;
                if(Objects.equals(s, "2")){
                    stopService(intent);
                }else startService(intent);
            }
        });
        if(!Objects.equals(a,"2")){startService(intent);}


    }

    @Override
    protected void onStop() {
        super.onStop();

        stopService(intent);

    }


}
