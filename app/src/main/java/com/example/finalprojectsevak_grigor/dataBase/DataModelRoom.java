package com.example.finalprojectsevak_grigor.dataBase;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.example.finalprojectsevak_grigor.Constants;

@Entity(tableName = Constants.Table1)
public class DataModelRoom {
    @PrimaryKey(autoGenerate = true)

    private int id;

    private String json;

  private String type;


    public DataModelRoom(String json, String type) {
        this.json = json;
        this.type = type;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
