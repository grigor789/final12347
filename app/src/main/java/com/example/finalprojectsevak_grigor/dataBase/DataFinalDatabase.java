package com.example.finalprojectsevak_grigor.dataBase;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
@Database(entities ={DataModelRoom.class},version = 1)
public abstract class DataFinalDatabase extends RoomDatabase {

    private static DataFinalDatabase roomDb;

    private static final String Database_name="database";
    public synchronized static DataFinalDatabase getInstance(Context context){
        if(roomDb==null){roomDb= Room.databaseBuilder(context.getApplicationContext(),DataFinalDatabase.class,Database_name)
        .allowMainThreadQueries().build();}
    return roomDb;}
    public abstract DataDao dao();

}
