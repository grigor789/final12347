package com.example.finalprojectsevak_grigor.dataBase;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.finalprojectsevak_grigor.Constants;

import java.util.List;

@Dao
public interface DataDao {
    @Insert
    void addData(DataModelRoom dataModelRoom);

    @Update
    void updateData(DataModelRoom dataModelRoom);

    @Query("Delete  FROM " + Constants.Table1 + " WHERE json == :json")
    void deleteData(String json);

    @Query("SELECT * FROM " + Constants.Table1 + " ORDER BY ID ASC ")
    LiveData<List<DataModelRoom>> readData();

    @Query("DELETE FROM " + Constants.Table1)
    void deleteAll();

    @Query("SELECT * FROM " + Constants.Table1 + " WHERE json == :json")
    int checkExistence(String json);


}
